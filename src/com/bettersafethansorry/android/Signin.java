package com.bettersafethansorry.android;

import java.util.ArrayList;
import java.util.Collections;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


public class Signin extends Activity implements AnimationListener {
	final Context context = this;
	final Handler handler = new Handler();
	AlertDialogManager alert = new AlertDialogManager();
	
	ConnectionDetector cd;
	
	EditText primaryemail;
	EditText firstEmail;
    EditText secondEmail;
    EditText thirdEmail;
    EditText passcode;
    EditText confirmPasscode;
    EditText secretQuestion;
    EditText secretAnswer;
    
    TextView text;
    TextView italic_1;
    TextView italic_2;
    TextView italic_3;
    TextView titleText_1;
    TextView titleText_2;
    TextView titleText_3;
    TextView withbold_1;
    TextView withbold_2;
    TextView withbold_3;
    TextView withbold_4;
    TextView withbold_5;
    TextView withbold_6;
    TextView withbold_7;
    TextView withbold_8;
    TextView withbold_9;
    
    ImageView bacgroundImage;
    ImageView b_1;
    ImageView b_2;
    ImageView b_3;
    ImageView b_1a;
    ImageView b_2a;
    ImageView b_3a;
    
    LinearLayout linear;
    LinearLayout mainLayout;
    LinearLayout textIntroLayout;
    
    String twitterToken;
    String twiterSecret;
    
    SharedPreferences.Editor editor;
    SharedPreferences preferences;
    
    Animation animate;
    Animation fadeinStart;
    Animation fadeinScroll;
    Animation fadeoutStart;
    Animation fade_1;
    Animation fade_2;
    Animation fade_3;
    Animation fade_1a;
    Animation fade_2a;
    Animation fade_3a;
    Animation fadein1;
    Animation fadein2;
    Animation fadein3;
    
    MediaPlayer mp;
    Typeface face;
    
    ArrayList<Animation> sequence = new ArrayList<Animation>();
    
	Runnable TextAnimation = new Runnable() {

		public void run() {
			titleText_1.setVisibility(View.VISIBLE);
			titleText_1.startAnimation(fadein1);
            titleText_2.startAnimation(fadein2);
            titleText_3.startAnimation(fadein3);
            
            handler.postDelayed(SlideAnimation, 7500);
		} 
	};

	
	Runnable SlideAnimation = new Runnable() {
		
		public void run() {
			textIntroLayout.startAnimation(fadeoutStart);
			Collections.shuffle(sequence);
		}
	};
	
	@Override
     public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        preferences = getSharedPreferences("BSTS", 0);
       
    	if(preferences.getString("BSTS_PRIMARYEMAIL", null) != null) {
    		Intent i = new Intent(Signin.this, controlView.class);
        	startActivity(i);
        	finish();
    	} else {
    		
    		// LINEAR LAYOUT
    		linear = (LinearLayout) findViewById(R.id.linear);
            mainLayout = (LinearLayout) findViewById(R.id.mainLayout);
            textIntroLayout = (LinearLayout) findViewById(R.id.textIntroLayout);
            
            // EDITTEXT
            primaryemail = (EditText) findViewById(R.id.primaryemail);
            firstEmail = (EditText) findViewById(R.id.firstemail);
            secondEmail = (EditText) findViewById(R.id.secondemail);
            thirdEmail = (EditText) findViewById(R.id.thirdemail);
            passcode = (EditText) findViewById(R.id.passcode);
            confirmPasscode = (EditText) findViewById(R.id.confirm_passcode);
            secretQuestion = (EditText) findViewById(R.id.secret_question);
            secretAnswer = (EditText) findViewById(R.id.secret_answer);
            
            // IMAGE VIEW
            bacgroundImage = (ImageView) findViewById(R.id.scroll_image);
            
            b_1 = (ImageView) findViewById(R.id.b1);
            b_2 = (ImageView) findViewById(R.id.b2);
            b_3 = (ImageView) findViewById(R.id.b3);
            b_1a = (ImageView) findViewById(R.id.b1a);
            b_2a = (ImageView) findViewById(R.id.b2a);
            b_3a = (ImageView) findViewById(R.id.b3a);
            
            // TEXT VIEW
            titleText_1 = (TextView) findViewById(R.id.titleText_1);
            titleText_2 = (TextView) findViewById(R.id.titleText_2);
            titleText_3 = (TextView) findViewById(R.id.titleText_3);
            
            italic_1 = (TextView) findViewById(R.id.italic_1);
            italic_2 = (TextView) findViewById(R.id.italic_2);
            italic_3 = (TextView) findViewById(R.id.italic_3);
            
            withbold_1 = (TextView) findViewById(R.id.withbold_1);
            withbold_2 = (TextView) findViewById(R.id.withbold_2);
            withbold_3 = (TextView) findViewById(R.id.withbold_3);
            withbold_4 = (TextView) findViewById(R.id.withbold_4);
            withbold_5 = (TextView) findViewById(R.id.withbold_5);
            withbold_6 = (TextView) findViewById(R.id.withbold_6);
            withbold_7 = (TextView) findViewById(R.id.withbold_7);
            withbold_8 = (TextView) findViewById(R.id.withbold_8);
            withbold_9 = (TextView) findViewById(R.id.withbold_9);
            
            //CUSTOM TEXT
            italic_3.setText(Html.fromHtml("When successfully enabled <strong>BetterSafe</strong> will 'quitely run' in the background with full functionality until disabled."));
            withbold_1.setText(Html.fromHtml("Play it safe and enable <strong>BetterSafe</strong> - an automated Time-Stamped and GPS-Tagged Safety and Monitoring System that allows you to share your current time-location with your loved ones. "));
            withbold_2.setText(Html.fromHtml("<strong>BetterSafe</strong> automatically sends out email message with time and current location tags every 5 minutes to your registered Friends and Family."));
            withbold_3.setText(Html.fromHtml("It also automatically sends messages view your personal Twitter account a time tag and a screen-grab map of your location every 15 minutes. This tells your Twitter followers your exact location at specific times. And with hashtags <strong>#FYIfriends</strong> and <strong>#BetterSafeThanSorry</strong>, all tweets are easily compiled and identified."));
            withbold_4.setText(Html.fromHtml("Make sure you have a mobile broadband subscription activated whenever you enable <strong>BetterSafe</strong>. "));
            withbold_5.setText(Html.fromHtml("<strong>BetterSafe</strong> is a great way to let friends and family watch over you wherever, whenever."));
            withbold_6.setText(Html.fromHtml("Enter the <strong>VALID</strong> email addresses of 3 of your most reliable friends and family."));
            withbold_7.setText(Html.fromHtml("Enter and log in to your Twitter Account And stay logged in while <strong>BetterSafe</strong> is enabled. If you don't have Twitter account, it is highly recommended that you create and register one."));
            withbold_8.setText(Html.fromHtml("<h1>REMEMBER</h1> \n When you ENABLE <strong>BetterSafe</strong>, people may know exactly where you are, at specific times. And you may want them to know your location for you to feel safe. <strong>BUT</strong> when you find yourself in a safe environment, please DISABLE <strong>BetterSafe</strong> to stop the automated messages."));
            withbold_9.setText(Html.fromHtml("Enter a 4- Digit Passcode \n to enable and disable <strong>BetterSafe</strong>."));
            
            // load the animation
            fadeinScroll = AnimationUtils.loadAnimation(getApplicationContext(),
                    R.animator.fadeinscroll);
            
            fadeinStart = AnimationUtils.loadAnimation(getApplicationContext(),
                    R.animator.fadeinstart);
            
            fadeoutStart = AnimationUtils.loadAnimation(getApplicationContext(),
                    R.animator.fadeoutstart);
            
            animate = AnimationUtils.loadAnimation(getApplicationContext(),
                    R.animator.sequential);
            
            fade_1 = AnimationUtils.loadAnimation(getApplicationContext(),
                    R.animator.fade_1);
            
            fade_2 = AnimationUtils.loadAnimation(getApplicationContext(),
                    R.animator.fade_2);
            
            fade_3 = AnimationUtils.loadAnimation(getApplicationContext(),
                    R.animator.fade_3);
            
            fade_1a = AnimationUtils.loadAnimation(getApplicationContext(),
                    R.animator.fade_1a);
            
            fade_2a = AnimationUtils.loadAnimation(getApplicationContext(),
                    R.animator.fade_2a);
            
            fade_3a = AnimationUtils.loadAnimation(getApplicationContext(),
                    R.animator.fade_3a);
            
            fadein1 = AnimationUtils.loadAnimation(getApplicationContext(),
                    R.animator.fadein_1);
            
            fadein2 = AnimationUtils.loadAnimation(getApplicationContext(),
                    R.animator.fadein_2);
            
            fadein3 = AnimationUtils.loadAnimation(getApplicationContext(),
                    R.animator.fadein_3);
            
            // SEQUENCE ARRAYLIST
            sequence.add(fade_1);
			sequence.add(fade_2);
			sequence.add(fade_3);
			sequence.add(fade_1a);
			sequence.add(fade_2a);
			sequence.add(fade_3a);
			
            // set animation listener
            animate.setAnimationListener(this);
            fadeinStart.setAnimationListener(this);
            fadeinScroll.setAnimationListener(this);
            fadeoutStart.setAnimationListener(this);
            fade_1.setAnimationListener(this);
            fade_2.setAnimationListener(this);
            fade_3.setAnimationListener(this);
            fade_1a.setAnimationListener(this);
            fade_2a.setAnimationListener(this);
            fade_3a.setAnimationListener(this);
            fadein1.setAnimationListener(this);
            fadein2.setAnimationListener(this);
            fadein3.setAnimationListener(this);

            // CALL ANIMATION SEQUENCE
			handler.postDelayed(TextAnimation, 100);
            
            for(int i = 0; i < linear.getChildCount(); i++) {
                View v = linear.getChildAt(i);
                if(v instanceof TextView) {
                	face = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeue.ttf");
                    ((TextView)v).setTypeface(face);
                }
            }
            
            for(int i = 0; i < mainLayout.getChildCount(); i++) {
                View v = mainLayout.getChildAt(i);
                v.setVisibility(View.GONE);
            }
            
            // FONT FACE 
            face = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeueItalic.ttf");
            titleText_1.setTypeface(face);
            titleText_2.setTypeface(face);
            titleText_3.setTypeface(face);
            
            italic_1.setTypeface(face);
            italic_2.setTypeface(face);
            italic_3.setTypeface(face);
            
    	}
    }
    
	@Override
    public void onAnimationEnd(Animation animation) {
        // Take any action after completing the animation
    	if(animation  == animate) {
    		bacgroundImage.startAnimation(animate);
    	}
    	
    	if(animation == sequence.get(0)) {
    		b_1.startAnimation(sequence.get(0));
    	}
    	
    	if(animation == sequence.get(1)) {
    		b_2.startAnimation(sequence.get(1));
    	}
    	
    	if(animation == sequence.get(2)) {
    		b_3.startAnimation(sequence.get(2));
    	}
    	
    	if(animation == sequence.get(3)) {
    		b_1a.startAnimation(sequence.get(3));
    	}
    	
    	if(animation == sequence.get(4)) {
    		b_2a.startAnimation(sequence.get(4));
    	}
    	
    	if(animation == sequence.get(5)) {
    		b_3a.startAnimation(sequence.get(5));
    	}
    	
    	if(animation == fadeoutStart) {
    		textIntroLayout.setVisibility(View.GONE);
    		bacgroundImage.setVisibility(View.VISIBLE);
			bacgroundImage.startAnimation(fadeinStart);
			
			for(int i = 0; i < mainLayout.getChildCount(); i++) {
                View v = mainLayout.getChildAt(i);
                v.setVisibility(View.VISIBLE);
                v.startAnimation(fadeinScroll);
            }
    	}
    }
 
    @Override
    public void onAnimationRepeat(Animation animation) {
        // TODO Auto-generated method stub
    }
 
    @Override
    public void onAnimationStart(Animation animation) {
        // TODO Auto-generated method stub
    	
    	if(animation == fadeinStart) {
    		bacgroundImage.startAnimation(animate);
			b_1.setVisibility(View.VISIBLE);
			
            b_1.startAnimation(sequence.get(0));
			b_2.startAnimation(sequence.get(1));
			b_3.startAnimation(sequence.get(2));
			
			b_1a.startAnimation(sequence.get(3));
			b_2a.startAnimation(sequence.get(4));
			b_3a.startAnimation(sequence.get(5));		
    	}
    	
    	if(animation == fadein1) {
    		titleText_1.setVisibility(View.VISIBLE);
    		mp = MediaPlayer.create(this, R.raw.music);
    		mp.setLooping(true);
    		mp.setVolume(100,100);
    		mp.start();
    	}
    	
		if(animation == fadein2) {
			titleText_2.setVisibility(View.VISIBLE);
		}
		
		if(animation == fadein3) {
			titleText_3.setVisibility(View.VISIBLE);
		}
		
		if(animation == sequence.get(0)) {
    		b_1.setVisibility(View.VISIBLE);
    	}
    	
    	if(animation == sequence.get(1)) {
    		b_2.setVisibility(View.VISIBLE);
    	}
    	
    	if(animation == sequence.get(2)) {
    		b_3.setVisibility(View.VISIBLE);
    	}
    	
    	if(animation == sequence.get(3)) {
    		b_1a.setVisibility(View.VISIBLE);
    	}
    	
    	if(animation == sequence.get(4)) {
    		b_2a.setVisibility(View.VISIBLE);
    	}
    	
    	if(animation == sequence.get(5)) {
    		b_3a.setVisibility(View.VISIBLE);
    	}
    }
    
    public void onStop() {
    	super.onStop();
    	mp.pause();
    }
    
    public void onPause() {
    	super.onPause();
    	mp.pause();
    }
    
    public void signUpToTwitter(View view) {
    	Intent i = new Intent(Signin.this, TwitterWebView.class);
    	startActivity(i);
    }
    
    public void onResume() {
    	super.onResume();
    	
    	if(mp != null) {
    		int length = mp.getCurrentPosition();
        	mp.seekTo(length);
        	mp.start();
    	}
    	
    	preferences = getSharedPreferences("BSTS", 0);
    	
    	if(preferences.getBoolean("BSTS_TWITTER_AUTH", false) == true) {
    		Button TwitterLink = (Button) findViewById(R.id.twlink);
    		Button TwitterConnectedButton = (Button) findViewById(R.id.twconnected);
    		
    		TwitterLink.setVisibility(View.GONE);
    		TwitterConnectedButton.setVisibility(View.VISIBLE);
    		
    		Toast.makeText(getApplicationContext(), "Successfully Connected to Twitter", Toast.LENGTH_LONG).show();
    	}
    	
    	Log.d("Important","Resume");
    }
    
    public void signup(View view) {
    	String finalPrimaryEmail = primaryemail.getText().toString();
    	String finalFirstEmail = firstEmail.getText().toString();
    	String finalSecondEmail = secondEmail.getText().toString();
    	String finalThirdEmail = thirdEmail.getText().toString();
    	String finalPasscode = passcode.getText().toString();
    	String finalConfirmPasscode = confirmPasscode.getText().toString();
    	
    	if(finalPrimaryEmail.matches("")) {
			alert.showAlertDialog(context, "Required!",
                    "Please Enter an Email Address", false);
			primaryemail.requestFocus();
		} else if(finalFirstEmail.matches("")) {
    		alert.showAlertDialog(context, "Required!",
                    "Please Enter an Email on First Email Field", false);
    		firstEmail.requestFocus();
    		
    	} else if(finalPasscode.matches("")) {
    		alert.showAlertDialog(context, "Required!",
                    "Please Enter Your Unique Passcode", false);
    		
    		passcode.requestFocus();
    	} else if(finalConfirmPasscode.matches("")) {
    		alert.showAlertDialog(context, "Required!",
                    "Please Confrim Your Unique Passcode", false);
    		
    		confirmPasscode.requestFocus();
    	} else if(!finalPasscode.equals(finalConfirmPasscode)) {
    		alert.showAlertDialog(context, "Important",
                    "Please Check your Passcode, Your Passcode and Confirm Passcode must match.", false);
    		
    		confirmPasscode.requestFocus();
    	} else {
    		saveUserPref(finalFirstEmail, finalSecondEmail, finalThirdEmail, finalPrimaryEmail, finalConfirmPasscode);
    		Intent i = new Intent(Signin.this, controlView.class);
        	startActivity(i);
        	finish();
    	}
    }
    
    public void saveUserPref(String finalFirstEmail, String finalSecondEmail, String finalThirdEmail, String finalPrimaryEmail, String passcode) {	
    	 editor = getSharedPreferences("BSTS", 0).edit();
    	 editor.putString("BSTS_PRIMARYEMAIL", finalPrimaryEmail);
    	 editor.putString("BSTS_FIRSTEMAIL", finalFirstEmail);
    	 editor.putString("BSTS_SECONDEMAIL", finalSecondEmail);
    	 editor.putString("BSTS_THIRDEMAIL", finalThirdEmail);
    	 editor.putString("BSTS_PASSCODE", passcode);
    	 editor.putString("BSTS_QUESTION", secretQuestion.getText().toString());
    	 editor.putString("BSTS_ANSWER", secretAnswer.getText().toString());
    	 editor.commit();	 
    }
}

