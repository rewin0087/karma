package com.bettersafethansorry.android;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;


public class TwitterWebView extends Activity {
	WebView web;
	ProgressDialog progrDialog;
	final Context context = this;
	
    @Override
     public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.twitter_web_view);
        progrDialog = new ProgressDialog(context);
        progrDialog.setMessage("Loading...");
        progrDialog.show();
        
        web = (WebView)findViewById(R.id.twitter_signup);
        web.loadUrl("http://bettersafe-thansorry.com/android/data/?login=true");
        web.setWebViewClient(new WebViewClient() {
        	
        	@Override  
            public void onPageFinished(WebView view, String url) { 
                String currentUrl = web.getUrl();
                Log.i("current", "current Url is: " + currentUrl);
                progrDialog.dismiss();
                
                // Lets check if we have access token and secret
                if(currentUrl.matches("(.*)ACCESS_TOKEN(.*)") && currentUrl.matches("(.*)ACCESS_SECRET(.*)") && currentUrl.matches("(.*)SUCCESS_AUTH(.*)")) {
                	Uri link = Uri.parse(currentUrl);
                	
                	// retrieve access token
                	String token = link.getQueryParameter("ACCESS_TOKEN");
                	// retrieve access secret
                	String secret = link.getQueryParameter("ACCESS_SECRET");
                	
                	Log.i("Done", "token: " + token + " secret: "+secret);
                	if(!token.matches("") && !secret.matches("")) {
                		// store token and secret
                		 Editor editor = getSharedPreferences("BSTS", 0).edit();
                		 editor.putString("BSTS_TWITTER_ACCESS_TOKEN", token);
                		 editor.putString("BSTS_TWITTER_ACCESS_SECRET", secret);
	                   	 editor.putBoolean("BSTS_TWITTER_AUTH", true);
	                   	 editor.commit();
	                   	 
	                   	 TwitterWebView.this.finish();
                	}
                }
            }
        	
        	@Override
	        public boolean shouldOverrideUrlLoading(WebView view, String url) { 
	        	super.shouldOverrideUrlLoading(view, url);
	            return false;
	        }
        }); 
        	
    }
}

