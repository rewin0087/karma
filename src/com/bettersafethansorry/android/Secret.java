package com.bettersafethansorry.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;


public class Secret extends Activity {
	final Context context = this;
	SharedPreferences preferences;
	AlertDialogManager alert = new AlertDialogManager();
	
	EditText secretQuestion;
	EditText secretAnswer;
	
    @Override
     public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.secret);
        
        secretQuestion = (EditText) findViewById(R.id.recovery_secret_question);
        secretAnswer = (EditText) findViewById(R.id.recovery_secret_answer);
        
    }
    
    public void Recover(View view) {
    	preferences = getSharedPreferences("BSTS", 0);
    	String finalSecretQuestion = secretQuestion.getText().toString();
    	String finalSecretAnswer = secretAnswer.getText().toString();
    	
    	if(finalSecretQuestion.matches(preferences.getString("BSTS_QUESTION", null)) && finalSecretAnswer.matches(preferences.getString("BSTS_ANSWER", null))) {
    		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
       	 
    		 alertDialogBuilder.setTitle("Success")
    			.setMessage("Your Passcode: " + preferences.getString("BSTS_PASSCODE", null))
    			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
    				public void onClick(DialogInterface dialog, int id) {
    					Secret.this.finish();
    				}
    		});
    		
    		 // create alert dialog
    		 AlertDialog alertDialog = alertDialogBuilder.create();
    		 // show it
    		 alertDialog.show();
    	} else {
    		alert.showAlertDialog(context, "Error",
                    "Please Enter Your Correct Secret Question and Secret Answer.", false);
    	}
       	 
    }
}

