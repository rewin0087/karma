package com.bettersafethansorry.android;

import java.io.IOException;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation.AnimationListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


public class controlView extends Activity implements AnimationListener {

	final Context context = this;
	final Handler handler = new Handler();
	AlertDialogManager alert = new AlertDialogManager();
	
	SharedPreferences preferences;
	SharedPreferences.Editor edit;
	GPSTracker gps;
	
	ImageButton enableButton;
	ImageButton disableButton;
	
	EditText passText;
	
	TextView titleText_1;
    TextView titleText_2;
    TextView titleText_3;
    
    LinearLayout mainLayout;
    LinearLayout textSlide;
    
	HttpResponse response;
	HttpClient httpclient;
	HttpPost httppost;
	
	URLConnection connection;
	SendPostRequest Serverrequest;
	List<NameValuePair> nameValuePairs;
	
	double latitude;
	double longitude;
	
	String location;
	String stringLatitude;
	String stringLongitude;
	String stringPrimaryEmail;
	String stringFirstEmail;
	String stringSecondEmail;
	String stringThirdEmail;
	String stringAccessToken;
	String stringAccessSecret;
	String CurrentDate;
	
	Animation fadein1;
    Animation fadein2;
    Animation fadein3;
    Animation fadeinLayout;
    Animation fadeoutStart;
    
	int tries = 0;
	LocationManager locationManager;
	MediaPlayer mp;
	ConnectionDetector cd;
	
	public static int notifyId = 999;
    
    @Override
     public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.control_view);
        
        preferences = getSharedPreferences("BSTS", 0);
        
        if(preferences.getString("BSTS_PRIMARYEMAIL", null) == null) {
        	edit = preferences.edit();
		    edit.clear().commit();
		    	
    		Intent i = new Intent(controlView.this, Signin.class);
        	startActivity(i);
        	controlView.this.finish();
        	
    	} else {
    		// Linear Layout
    		mainLayout = (LinearLayout) findViewById(R.id.mainLayout);
    		textSlide = (LinearLayout) findViewById(R.id.textSlide);
    		
    		// TEXT VIEW
            titleText_1 = (TextView) findViewById(R.id.titleText_1);
            titleText_2 = (TextView) findViewById(R.id.titleText_2);
            titleText_3 = (TextView) findViewById(R.id.titleText_3);
            
            // Image Button
    		enableButton = (ImageButton) findViewById(R.id.enableTrack);
    		disableButton = (ImageButton) findViewById(R.id.disableTrack);
    		
    		// Edit Text
    		passText = (EditText) findViewById(R.id.pass_text);
	        passText.setFocusable(true);
	        passText.requestFocus();
	        
	        disableButton.setClickable(false);
	        
	        // Load Aniamtion 
	        fadein1 = AnimationUtils.loadAnimation(getApplicationContext(),
                    R.animator.fadein_1);
            
            fadein2 = AnimationUtils.loadAnimation(getApplicationContext(),
                    R.animator.fadein_2);
            
            fadein3 = AnimationUtils.loadAnimation(getApplicationContext(),
                    R.animator.fadein_3);
            
            fadeinLayout = AnimationUtils.loadAnimation(getApplicationContext(),
                    R.animator.fadeinscroll);
            
            fadeoutStart = AnimationUtils.loadAnimation(getApplicationContext(),
                    R.animator.fadeoutstart);
            
            // Animation Listener
            fadein1.setAnimationListener(this);
            fadein2.setAnimationListener(this);
            fadein3.setAnimationListener(this);
            fadeoutStart.setAnimationListener(this);
            
            // Start Animation
			titleText_1.startAnimation(fadein1);
            titleText_2.startAnimation(fadein2);
            titleText_3.startAnimation(fadein3);
            
            // Hide Item inside mainLayout
            for(int i = 0; i < mainLayout.getChildCount(); i++) {
                View v = mainLayout.getChildAt(i);
                v.setVisibility(View.GONE);
            }
            
            gps = new GPSTracker(controlView.this);
            if(gps.canGetLocation()){
            	latitude = gps.getLatitude();
                longitude = gps.getLongitude();
                location = gps.getAddressLine(context);
                
            }else{
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert(); 
            }
    	}
    }
    
    @Override
    public void onAnimationEnd(Animation animation) {
    	if(animation == fadein3) {
    		// fade text Slide
    		textSlide.startAnimation(fadeoutStart);

    		// Hide Item inside mainLayout
            for(int i = 0; i < mainLayout.getChildCount(); i++) {
                View v = mainLayout.getChildAt(i);
                v.setVisibility(View.VISIBLE);
                v.startAnimation(fadeinLayout);
            }
            
    	}
    	
    	if(animation == fadeoutStart) {
    		// hide Text Slide
    		textSlide.setVisibility(View.GONE);
    		
    		// show location
            if(location == null) {
            	Toast.makeText(getApplicationContext(), "We cant find your Location...", Toast.LENGTH_LONG).show();
            } else {
            	Toast.makeText(getApplicationContext(), "Current Location: "+location, Toast.LENGTH_LONG).show();
            }
            
            passText.setFocusable(true);
    	}
    }
    
    @Override
    public void onAnimationRepeat(Animation animation) {}
    
    @Override
    public void onAnimationStart(Animation animation) {
    	if(animation == fadein1) {
    		mp = MediaPlayer.create(this, R.raw.music);
    		mp.setLooping(true);
    		mp.setVolume(100,100);
    		mp.start();
    	}
    	
    }
    
    public void onResume() {
    	super.onResume();
    	
    	if(mp != null) {
    		int length = mp.getCurrentPosition();
        	mp.seekTo(length);
        	mp.start();
    	}
    	
    	preferences = getSharedPreferences("BSTS", 0);
        
        if(preferences.getString("BSTS_PRIMARYEMAIL", null) == null) {
        	edit = preferences.edit();
		    edit.clear().commit();
        	controlView.this.finish();	
    	} else {
    		gps = new GPSTracker(controlView.this);
            if(gps.canGetLocation()){
            	latitude = gps.getLatitude();
                longitude = gps.getLongitude();
                location = gps.getAddressLine(context);
                
            }else{
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert(); 
            }
            
            if(preferences.getBoolean("BSTS_Enable", false) == true) {
            	SetDisableButtonClickable();
            } else {
            	SetEnableButtonClickable();
            }
    	}
    }
    
    public void onPause() {
    	super.onPause();
    	mp.pause();
    }
    
    public void onStop() {
    	super.onStop();
    	
    	mp.pause();
    	gps.stopUsingGPS();
    }
    
    Runnable SendTweet = new Runnable() {

		public void run() {
			cd = new ConnectionDetector(getApplicationContext());
			// Check if Internet present
            if (!cd.isConnectingToInternet()) {
                // stop executing code by return
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        		
        		alertDialogBuilder.setTitle("Internet Connection Error")
        			.setMessage("Please connect to working Internet connection")
        			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
        				public void onClick(DialogInterface dialog, int id) {
        					controlView.this.finish();
        				}
        			});
        		
        		// create alert dialog
        		AlertDialog alertDialog = alertDialogBuilder.create();

        		// show it
        		alertDialog.show();
            } else {
            	SetPostTweets();
            	Notification("tweet");
    			handler.postDelayed(this, 900000);
            }
	    }
    }; 
    
    
    Runnable SendEmail = new Runnable() {

		public void run() {
			cd = new ConnectionDetector(getApplicationContext());
			// Check if Internet present
            if (!cd.isConnectingToInternet()) {
                // stop executing code by return
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        		
        		alertDialogBuilder.setTitle("Internet Connection Error")
        			.setMessage("Please connect to working Internet connection")
        			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
        				public void onClick(DialogInterface dialog, int id) {
        					controlView.this.finish();
        				}
        			});
        		
        		// create alert dialog
        		AlertDialog alertDialog = alertDialogBuilder.create();

        		// show it
        		alertDialog.show();
            } else {
            	SetPostEmails();
            	Notification("email");
    			handler.postDelayed(this, 300000);
            }
	    }
    }; 
    
    public void SetPostTweets() {
    	if(preferences.getString("BSTS_PRIMARYEMAIL", null) == null) { 
    		Intent i = new Intent(controlView.this, Signin.class);
        	startActivity(i);
        	finish();
    	}
        
        preferences = getSharedPreferences("BSTS", 0);
        
        // Get preferences data
        stringPrimaryEmail = preferences.getString("BSTS_PRIMARYEMAIL", null);
        stringAccessToken = preferences.getString("BSTS_TWITTER_ACCESS_TOKEN", null);
        stringAccessSecret = preferences.getString("BSTS_TWITTER_ACCESS_SECRET", null);

        Time now = new Time();
        now.setToNow();
        CurrentDate = now.format("%H:%M:%S");
        
        // Add your data
        nameValuePairs = new ArrayList<NameValuePair>();
        
        if(stringAccessToken != null && stringAccessSecret != null && !stringAccessToken.matches("") == true && !stringAccessSecret.matches("") == true) {
        	nameValuePairs.add(new BasicNameValuePair("BSTS_ACCESS_TOKEN", stringAccessToken));
            nameValuePairs.add(new BasicNameValuePair("BSTS_ACCESS_SECRET", stringAccessSecret));
        }
        
        nameValuePairs.add(new BasicNameValuePair("BSTS_EMAIL_PRIMARY", stringPrimaryEmail));
        nameValuePairs.add(new BasicNameValuePair("BSTS_TIME", CurrentDate));
        nameValuePairs.add(new BasicNameValuePair("BSTS_TWEET", "true"));

        gps = new GPSTracker(controlView.this);
        // check if GPS enabled     
        if(gps.canGetLocation()){
        	latitude = gps.getLatitude();
            longitude = gps.getLongitude();
                        
            stringLatitude = Double.toString(latitude);
            stringLongitude = Double.toString(longitude);

        	nameValuePairs.add(new BasicNameValuePair("BSTS_LAT", stringLatitude));
            nameValuePairs.add(new BasicNameValuePair("BSTS_LONG", stringLongitude));
        }else{
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert(); 
        }
        
        Serverrequest = new SendPostRequest();
        Serverrequest.execute();
        
        try {
			Serverrequest.get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void SetPostEmails() {
    	if(preferences.getString("BSTS_PRIMARYEMAIL", null) == null) { 
    		Intent i = new Intent(controlView.this, Signin.class);
        	startActivity(i);
        	finish();
    	}
        
        preferences = getSharedPreferences("BSTS", 0);
        
        // Get preferences data
        stringPrimaryEmail = preferences.getString("BSTS_PRIMARYEMAIL", null);
        stringFirstEmail = preferences.getString("BSTS_FIRSTEMAIL", null);
        stringSecondEmail = preferences.getString("BSTS_SECONDEMAIL", null);
        stringThirdEmail = preferences.getString("BSTS_THIRDEMAIL", null);
        
        Time now = new Time();
        now.setToNow();
        CurrentDate = now.format("%H:%M:%S");
        
        // Add your data
        nameValuePairs = new ArrayList<NameValuePair>();
        
        nameValuePairs.add(new BasicNameValuePair("BSTS_EMAIL_PRIMARY", stringPrimaryEmail));
        nameValuePairs.add(new BasicNameValuePair("BSTS_FIRSTEMAIL", stringFirstEmail));
        nameValuePairs.add(new BasicNameValuePair("BSTS_SECONDEMAIL", stringSecondEmail));
        nameValuePairs.add(new BasicNameValuePair("BSTS_THIRDEMAIL", stringThirdEmail));
        nameValuePairs.add(new BasicNameValuePair("BSTS_TIME", CurrentDate));
        nameValuePairs.add(new BasicNameValuePair("BSTS_TWEET", "false"));
        
        gps = new GPSTracker(controlView.this);
        // check if GPS enabled     
        if(gps.canGetLocation()){
        	latitude = gps.getLatitude();
            longitude = gps.getLongitude();
                        
            stringLatitude = Double.toString(latitude);
            stringLongitude = Double.toString(longitude);

        	nameValuePairs.add(new BasicNameValuePair("BSTS_LAT", stringLatitude));
            nameValuePairs.add(new BasicNameValuePair("BSTS_LONG", stringLongitude));
        }else{
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert(); 
        }
        
        Serverrequest = new SendPostRequest();
        Serverrequest.execute();
        
        try {
			Serverrequest.get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void Notification (String type) {
    	++notifyId;
    	NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    	
    	NotificationCompat.Builder builder =  
    	        new NotificationCompat.Builder(this)  
    	        .setSmallIcon(R.drawable.ic_launcher);
    	
    	if(type.equalsIgnoreCase("email")) {
    		builder.setContentTitle("Better Safe - Email Sent")  
	        .setContentText("You are here  at "+location);
    	} else if(type.equalsIgnoreCase("tweet")) {
    		builder.setContentTitle("Better Safe - Tweet Sent")  
	        .setContentText("You are here  at "+location);
    	}
    	
    	builder.setAutoCancel(true);
    	
    	// Add as notification
    	manager.notify(notifyId, builder.build()); 
    }
    
    public class SendPostRequest extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... params) {
                httpclient = new DefaultHttpClient();
                httppost = new HttpPost("http://bettersafe-thansorry.com/android/data/");
                
                try {
                	
                	httppost.setHeader("Content-Type", "application/x-www-form-urlencoded");
                	httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                	httppost.setHeader("Accept-Charset", "utf-8");

                    // Execute HTTP Post Request
                    response = httpclient.execute(httppost);
                    
                    Log.v("Debug", response.getParams().toString());
                    Log.v("Debug", response.getStatusLine().toString());
                    Log.d("Done", "posting");
                    
                } catch (ClientProtocolException e) {
                    // TODO Auto-generated catch block
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                }
                
            return null;
        }
        
        @Override
        protected void onPostExecute(Void result) {
        	Toast.makeText(getApplicationContext(), "Current Location Sent", Toast.LENGTH_LONG).show();
        }
    }
    
    public void EnableTrack(View view) {
    	preferences = getSharedPreferences("BSTS", 0);
    	String passcode = preferences.getString("BSTS_PASSCODE", null);
    	
    	if(passcode.toString().equals(passText.getText().toString())) {
    		Toast.makeText(getApplicationContext(), "Sending...", Toast.LENGTH_LONG).show();   		
    		handler.postDelayed(SendEmail, 2000);
    		handler.postDelayed(SendTweet, 2000);
    		
    		edit = getSharedPreferences("BSTS", 0).edit();
       	 	edit.putBoolean("BSTS_Enable", true);
       	 	edit.commit();
       	 	
    		passText.setText("");
        	
        	SetDisableButtonClickable();
    	} else {
    		InvalidPasscode();
    		alert.showAlertDialog(context, "Important",
                    "Wrong Passcode, Please Enter your correct Passcode", false);
    	}
    	
    }
    
    public void DisableTrack(View view) {
    	preferences = getSharedPreferences("BSTS", 0);
    	String passcode = preferences.getString("BSTS_PASSCODE", null);
    	
    	if(passcode.toString().equals(passText.getText().toString())) {
    		Toast.makeText(getApplicationContext(), "Stopping...", Toast.LENGTH_LONG).show(); 
        	handler.removeCallbacks(SendEmail);
        	handler.removeCallbacks(SendTweet);
        	
        	edit = getSharedPreferences("BSTS", 0).edit();
       	 	edit.putBoolean("BSTS_Enable", false);
       	 	edit.commit();
       	 	
        	passText.setText("");
        	SetEnableButtonClickable();
        	
    	} else {
    		InvalidPasscode();
    		alert.showAlertDialog(context, "Important",
                    "Wrong Passcode, Please Enter your correct Passcode", false);
    	}
    }
    
    public void SetEnableButtonClickable() {
    	enableButton.setImageResource(R.drawable.black_enable);
    	enableButton.setClickable(true);
    	disableButton.setImageResource(R.drawable.white_disable);
    	disableButton.setClickable(false);
    }
    
    public void SetDisableButtonClickable() {
    	disableButton.setImageResource(R.drawable.black_disable);
    	disableButton.setClickable(true);
    	enableButton.setImageResource(R.drawable.white_enable);
    	enableButton.setClickable(false);
    }
    
    public void Settings(View view) {
    	preferences = getSharedPreferences("BSTS", 0);
    	String passcode = preferences.getString("BSTS_PASSCODE", null);
    	
    	if(passcode.toString().equals(passText.getText().toString())) {
    		Toast.makeText(getApplicationContext(), "Passcode Accepted", Toast.LENGTH_LONG).show();   		
    		
    		passText.setText("");
    		Intent i = new Intent(controlView.this, Settings.class);
         	startActivity(i);
         	
    	} else {
    		if(tries > 2) {
    			alert.Close();
    		}
    		
    		InvalidPasscode();
    		alert.showAlertDialog(context, "Important",
                    "Please Enter your Passcode", false);
    		
    	}
    	
    }

    public void InvalidPasscode() {
    	tries = tries + 1;
    	edit = preferences.edit();
    	
		edit.putInt("BSTS_TRIES", tries);
		
    	if(tries > 2) {
    		alert.Close();
    		tries = 0;
    		Intent i = new Intent(controlView.this, Secret.class);
         	startActivity(i);
    	}
    }
}

