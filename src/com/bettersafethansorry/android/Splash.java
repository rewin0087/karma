package com.bettersafethansorry.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;


public class Splash extends Activity {
	Handler handler = new Handler();
	
	Runnable Start = new Runnable() {

		public void run() {
			Intent intent = new Intent(Splash.this, controlView.class);
			startActivity(intent);
			finish();
		} 
	};
	
	@Override
     public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
       
        handler.postDelayed(Start, 3000);
    }
    
}

