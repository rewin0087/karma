package com.bettersafethansorry.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;


public class Settings extends Activity {
	final Context context = this;
	SharedPreferences preferences;
	SharedPreferences.Editor editor;
	AlertDialogManager alert = new AlertDialogManager();
	ProgressDialog progrDialog;
	
	EditText firstEmail;
	EditText secondEmail;
	EditText thirdEmail;
	EditText currentPasscode;
	EditText newPasscode;
	EditText confirmPasscode;
	EditText secretQuestion;
	EditText secretAnswer;
	
    @Override
     public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
        
        preferences = getSharedPreferences("BSTS", 0);
        
        firstEmail = (EditText) findViewById(R.id.change_firstemail);
        secondEmail = (EditText) findViewById(R.id.change_secondemail);
        thirdEmail = (EditText) findViewById(R.id.change_thirdemail);
        currentPasscode = (EditText) findViewById(R.id.current_passcode);
        newPasscode = (EditText) findViewById(R.id.new_passcode);
        confirmPasscode = (EditText) findViewById(R.id.confirm_passcode);
        secretQuestion = (EditText) findViewById(R.id.secret_question);
        secretAnswer = (EditText) findViewById(R.id.secret_answer);
     
        // Set Values from shared
        firstEmail.setText(preferences.getString("BSTS_FIRSTEMAIL", null));
        secondEmail.setText(preferences.getString("BSTS_SECONDEMAIL", null));
        thirdEmail.setText(preferences.getString("BSTS_THIRDEMAIL", null));
        currentPasscode.setText(preferences.getString("BSTS_PASSCODE", null));
        secretQuestion.setText(preferences.getString("BSTS_QUESTION", null));
        secretAnswer.setText(preferences.getString("BSTS_ANSWER", null));
        
        InputMethodManager imm =  (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(firstEmail, InputMethodManager.SHOW_IMPLICIT);
        Log.d("Important","Control");
    }
    
    public void signUpToTwitter(View view) {
    	Intent i = new Intent(Settings.this, TwitterWebView.class);
    	startActivity(i);
    }
    
    public void onResume() {
    	super.onResume();
    	
    	preferences = getSharedPreferences("BSTS", 0);
    	
    	if(preferences.getBoolean("BSTS_TWITTER_AUTH", false) == true) {
    		Button TwitterLink = (Button) findViewById(R.id.settings_twlink);
    		Button TwitterConnectedButton = (Button) findViewById(R.id.settings_twconnected);
    		
    		TwitterLink.setVisibility(View.GONE);
    		TwitterConnectedButton.setVisibility(View.VISIBLE);
    		
    	}
    	
    	Log.d("Important","Resume");
    }
    
    public void Signout(View view) {
    	AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
 		
 		alertDialogBuilder.setTitle("Sign out")
 			.setMessage("Are you sure you want to Sign out?")
 			.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
 				public void onClick(DialogInterface dialog, int id) {				
 					editor = getSharedPreferences("BSTS", 0).edit();
 					editor.clear().commit();	
 					Settings.this.finish();
 					
 			    	Intent i = new Intent(Settings.this, Signin.class);
 		        	startActivity(i);
 		        	
 				}
 			}).setNegativeButton("No", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.cancel();
				}
			});
 		
 		// create alert dialog
 		AlertDialog alertDialog = alertDialogBuilder.create();

 		// show it
 		alertDialog.show();  	
    }
    
    public void Save(View view) {
    	String finalFirstEmail = firstEmail.getText().toString();
    	String finalSecondEmail = secondEmail.getText().toString();
    	String finalThirdEmail = thirdEmail.getText().toString();
    	String finalSecretQuestion = secretQuestion.getText().toString();
    	String finalSecretAnswer = secretAnswer.getText().toString();
    	String finalNewPasscode = newPasscode.getText().toString();
    	String finalConfirmPasscode = confirmPasscode.getText().toString();
    	
    	if(finalFirstEmail.matches("")) {
    		alert.showAlertDialog(context, "Important",
                    "First Email must not empty.", false);
    		
    		firstEmail.requestFocus();
    	} else if(finalSecondEmail.matches("")) {
    		alert.showAlertDialog(context, "Important",
                    "Second Email must not empty.", false);
    		
    		secondEmail.requestFocus();
    	} else if(finalThirdEmail.matches("")) {
    		alert.showAlertDialog(context, "Important",
                    "Third Email must not empty.", false);
    		
    		thirdEmail.requestFocus();
    	} else {
    		progrDialog = new ProgressDialog(context);
            progrDialog.setMessage("Updating...");
            progrDialog.show();
            
    		editor = getSharedPreferences("BSTS", 0).edit();
       	 	editor.putString("BSTS_FIRSTEMAIL", finalFirstEmail);
       	 	editor.putString("BSTS_SECONDEMAIL", finalSecondEmail);
       	 	editor.putString("BSTS_THIRDEMAIL", finalThirdEmail);
       	 	editor.putString("BSTS_QUESTION", finalSecretQuestion);
       	 	editor.putString("BSTS_ANSWER", finalSecretAnswer);
       	 	
       	 	if((finalNewPasscode.matches("") == false && finalConfirmPasscode.matches("") == false) && finalNewPasscode.equalsIgnoreCase(finalConfirmPasscode) == true) {
       	 		editor.putString("BSTS_PASSCODE", finalConfirmPasscode);
       	 	}
       	 	
       	 	editor.commit();
       	 	
       	 AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
    	 
 		 alertDialogBuilder.setTitle("Success")
 			.setMessage("Successfully Update your Settings.")
 			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
 				public void onClick(DialogInterface dialog, int id) {
 					Settings.this.finish();
 					progrDialog.dismiss();
 				}
 		});
 		
 		 // create alert dialog
 		 AlertDialog alertDialog = alertDialogBuilder.create();
 		 // show it
 		 alertDialog.show();
    	}
    }
}

